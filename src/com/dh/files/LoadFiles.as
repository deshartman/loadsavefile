package com.dh.files
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.net.FileFilter;
	import flash.net.FileReference;
	import flash.utils.ByteArray;
	
	public class LoadFiles extends EventDispatcher
	{
		// Define the types of files that can be used
		public var textTypes:FileFilter = new FileFilter("Files (csv, txt)", "*.csv; *.txt");
	
		public function loadTheFile(fileReference:FileReference):void
		{
						
			fileReference.addEventListener(Event.SELECT, loadFile);
			fileReference.addEventListener(Event.OPEN, openHandler);
			fileReference.addEventListener(ProgressEvent.PROGRESS, progressHandler);
			fileReference.addEventListener(Event.COMPLETE, fileLoadCompleteHandler);
			fileReference.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
			
			fileReference.browse([textTypes]);
			trace("Browse Called");
		}
		
		public function loadFile(event:Event):void
		{
			trace("loadFile");
			var currentFile:FileReference = event.target as FileReference;
			
			currentFile.load();
		}
		
		public function openHandler(event:Event):void
		{
			trace("openHandler");
		}
		
		public function ioErrorHandler(event:IOErrorEvent):void
		{
			trace("ioErrorHandler");				
		}
		
		public function progressHandler(event:ProgressEvent):void
		{
			var currentFile:FileReference = event.target as FileReference;
			
			trace("progressHandler: name=" + currentFile.name + " bytesLoaded=" + event.bytesLoaded + " bytesTotal=" + event.bytesTotal);
		}
		
		public function fileLoadCompleteHandler(event:Event):void
		{
			trace("fileLoadCompleteHandler");
			
			var currentFile:FileReference = event.target as FileReference;
			
			trace("File name = " + currentFile.name);
			trace("File Size = " + currentFile.size);
			
			// access file data
			var data:ByteArray = currentFile.data;
			
			// indicate success
			dispatchEvent(new Event("rateSheetAdded"));
			
			trace("Event Dispatched");
			
		}
	}
}
package com.dh.files.tests
{
	import com.dh.files.LoadFiles;
	
	import flash.events.Event;
	import flash.events.ProgressEvent;
	import flash.net.FileFilter;
	import flash.net.FileReference;
	
	import flexunit.framework.Assert;
	import org.hamcrest.collection.array;
	
	import mockolate.*;
	import mockolate.runner.MockolateRunner;
	
	import org.flexunit.async.Async;
	
	MockolateRunner;

	[RunWith("mockolate.runner.MockolateRunner")]
	public class LoadFilesTests
	{
		
		private var fixture:LoadFiles;
		public var textTypes:FileFilter = new FileFilter("Files (csv, txt)", "*.csv; *.txt");
		
		[Mock] public var fileReferenceMock:FileReference;
		
		[Before]
		public function runBeforeEveryTest():void
		{
			fixture = new LoadFiles();
		}
		
		[After]
		public function runAfterEveryTest():void
		{
			fixture = null;
		}
		
		
		[Test(async)]
		public function loadTheFileTest():void
		{
			// Mock the functions
			mock(fileReferenceMock).method("browse").args([fixture.textTypes]).dispatches(new Event(Event.SELECT),100);
			
			mock(fileReferenceMock).method("load").noArgs()
				.dispatches(new ProgressEvent(ProgressEvent.PROGRESS, true,false,0,1000),200)
				.dispatches(new ProgressEvent(ProgressEvent.PROGRESS, true,false,500,1000),300)
				.dispatches(new Event(Event.COMPLETE),400);
			
			mock(fileReferenceMock).property("name").returns("testFile.csv");
			mock(fileReferenceMock).property("size").returns(1000);
			
			Async.proceedOnEvent(this, fixture, "rateSheetAdded", 1000,noEventFired);
			// No call the code
			fixture.loadTheFile(fileReferenceMock);
			
		}
		
		private function noEventFired(passthough:Object):void
		{
			Assert.fail(">>>>>>>  No event fired <<<<<<<");
		}
		
	}
}